coupling_interface="oasis_interface"

coupling_interface_path="${IOW_ESM_ROOT}/components/coupling_interface/build_${configuration}"

build_dir="build_${configuration}"
bin_dir="bin_${configuration}"

INCLUDES="-I${IOW_ESM_ROOT}/components/coupling_interface/build_${configuration} -I${IOW_ESM_NETCDF_INCLUDE}"

LIBS="-Wl,-rpath,${IOW_ESM_NETCDF_LIBRARY} -L${IOW_ESM_NETCDF_LIBRARY} -lnetcdf -lnetcdff -L $coupling_interface_path -lcoupling_interface"
#LIBS="-L$coupling_interface_path -lcoupling_interface"

if [ "$rebuild" == "rebuild" ]; then
	rm -r "${build_dir}"
    rm -r "${bin_dir}"
fi
mkdir -p ./"${build_dir}"
mkdir -p ./"${bin_dir}"

cd "${build_dir}"

$FC -c $FFLAGS ../src/helpers.F90 $INCLUDES

# compile binary
$FC $FFLAGS -o ../"${bin_dir}"/ROFF ../src/ROFF.F90 *.o $INCLUDES $LIBS 

cd -