program roff

use mod_coupling_interface, only: coupling_interface
use mod_coupling_cycle, only: coupling_cycle
use mod_action, only: send, receive
use mod_oasis_coupling_interface, only: oasis_coupling_interface

use mod_grid

use mod_log

implicit none

include 'mpif.h'

integer :: i, j, step, rank, ierror
character(5) :: coupler = "OASIS"
character(20) :: grid_file = "./mappings/t_grid.nc"

integer :: n_steps = 4464
integer :: coupling_timestep = 600

class(grid), allocatable :: full_grid
class(grid), allocatable :: my_grid

real, dimension(:,:), pointer :: rain
real, dimension(:,:), pointer :: runoff



! get the general coupling interface
class(coupling_interface), allocatable, target :: ci

class(coupling_cycle), allocatable :: cc

!!!!!!!!!!!!!!!! start program

! intialize MPI communication

call mpi_init(ierror)
if (ierror /= 0) then
    write(*,*) 'Failed to call mpi_init'
    stop
endif

! get global MPI rank
call mpi_comm_rank(MPI_COMM_WORLD, rank, ierror)   ! get my own rank (globally)
if (ierror /= 0) then
    write(*,*) 'Failed to call mpi_comm_rank'
    stop
endif

! initialize the specific coupling interface
if (trim(coupler) == "OASIS") then
    allocate(oasis_coupling_interface :: ci)
    ci = oasis_coupling_interface(comp_name='runoff', global_rank=rank, logfile_name="log.txt", verbosity_level=LOG_VERBOSITY_LEVEL_DEBUG)
else
    write(*,*) "Coupler ", coupler, " unknown. Abort."
    stop
endif

! read full grid
allocate(full_grid, source = grid(grid_file))

! get my portion of full grid
allocate(my_grid, source = full_grid)

if (.not. ci%add_grid(name=my_grid%name, shape=my_grid%shape, points=my_grid%points)) stop

allocate(rain(my_grid%shape(1), my_grid%shape(2)))
allocate(runoff(my_grid%shape(1), my_grid%shape(2)))

! add input fields with particular preparation methods to the interface
! if var in actions
if (.not. ci%add_input_field(grid_index=1, name="HRMRAI01", data=rain)) stop
if (.not. ci%add_output_field(grid_index=1, name="HSROFF01", data=runoff)) stop

if (.not. ci%complete_initialization()) stop

! initialize coupling cycle with valid actions that can be performed (hard coded)
!allocate(cc, source = coupling_cycle(["send_early", "receive_normal", "send_normal"]))

! define the coupling cycle's actions (can be done at runtime)
!if (.not. cc%add_action(receive("receive_normal", ["HRMRAI01"], ci))) stop
!if (.not. cc%add_action(send("send_normal", ["HSROFF01"], ci))) stop

! time loop

do step = 1, n_steps 
    call mpi_barrier(MPI_COMM_WORLD, i)

    ! send fields 
    !if (.not. cc%perform_action("send_early", current_time=(step-1)*coupling_timestep)) stop

    do i=1, ci%n_grids
        do j=1, ci%grids(i)%n_output_fields-1
            if (.not. ci%send_field(grid_index=i, field_index=j, current_time=(step-1)*coupling_timestep)) stop
        enddo
    enddo

    write(*,*) "Sent all early fields."

    ! receive fields 
    !if (.not. cc%perform_action("receive_normal", current_time=(step-1)*coupling_timestep)) stop

    do i=1, ci%n_grids
        do j=1, ci%grids(i)%n_input_fields
            if (.not. ci%receive_field(grid_index=i, field_index=j, current_time=(step-1)*coupling_timestep)) stop
        enddo
    enddo

    write(*,*) "Received all fields."
    
    ! do calculations here

    ! send fields 
    !if (.not. cc%perform_action("send_normal", current_time=(step-1)*coupling_timestep)) stop

    do i=1, ci%n_grids
        do j=ci%grids(i)%n_output_fields, ci%grids(i)%n_output_fields
            if (.not. ci%send_field(grid_index=i, field_index=j, current_time=(step-1)*coupling_timestep)) stop
        enddo
    enddo

    write(*,*) "Sent all fields."

enddo
if (.not. ci%terminate()) stop

end program